from django.contrib import admin
from .models import Todo
# Register your models here.

class TodoTable(admin.ModelAdmin):
    fields = ['task_name']

admin.site.register(Todo, TodoTable)
