from django.test import TestCase

from .models import Todo
# Create your tests here.


class DBTests(TestCase):
    def test_check_db(self):
        database = Todo.objects.all()
        assert database.count() == 0

    def test_add_task(self):
        k = Todo(task_name="Tests")
        k.save()
        t = Todo.objects.all()
        assert 1 == t.values()[0]['id']
        assert "Tests" == t.values()[0]['task_name']

